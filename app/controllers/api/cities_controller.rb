##
# An API controller class for the cities model.

class Api::CitiesController < ApplicationController
    ##
    # Renders all cities in JSON format.
    
    def index
        cities = City.all
        response = []
        
        # Loop through all cities.
        cities.each do |city|
            response.append(city.to_simple_json)
        end

        render json: response
    end

    ##
    # Renders a single city in JSON format.
    
    def show
        city = City.find(params[:id])
        render json: city.to_detailed_json
    end
end