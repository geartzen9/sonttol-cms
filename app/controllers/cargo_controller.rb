##
# A controller class for the cargos model.

class CargoController < ApplicationController
  before_action :authenticate_user!

  ##
  # Finds the correct instance that needs to be updated and shows the update form.

  def edit
    @cargo = Cargo.find(params[:id])
  end

  ##
  # Checks if the parameters are valid and updates the instance.

  def update
    @cargo = Cargo.find(params[:id])

    if @cargo.update(validate_params)
      redirect_to @cargo.passage
    else
      render :edit, status: :unprocessable_entity
    end
  end

  ##
  # Deletes the given cargo from the database.
  
  def destroy
    @cargo = Cargo.find(params[:id])
    @cargo.destroy

    redirect_to root_path, status: :see_other
  end

  ##
  # Validates the query parameters.

  private def validate_params
    params.require(:cargo).permit!
  end
end
