##
# A controller class for the cities model.

class CitiesController < ApplicationController
  before_action :authenticate_user!

  ##
  # Shows all cities from the database on a view.

  def index
    @cities = City.all
  end

  ##
  # Shows a single city from the database on a view.

  def show
    @city = City.find(params[:id])
  end

  ##
  # Creates a new instance of a city and shows the form.

  def new
    @city = City.new
  end

  ##
  # Creates a new city in the database and redirects to the the show view of that city.

  def create
    @city = City.new(validate_params)

    if @city.save
      redirect_to @city
    else
      render :new, status: :unprocessable_entity
    end
  end

  ##
  # Finds the correct instance that needs to be updated and shows the update form.

  def edit
    @city = City.find(params[:id])
  end

  ##
  # Checks if the parameters are valid and updates the instance.

  def update
    @city = City.find(params[:id])

    if @city.update(validate_params)
      redirect_to @city
    else
      render :edit, status: :unprocessable_entity
    end
  end

  ##
  # Deletes the given city from the database.
  
  def destroy
    @city = City.find(params[:id])
    @city.destroy

    redirect_to root_path, status: :see_other
  end

  ##
  # Shows the attach form for attaching a new skipper to a city.

  def show_attach_skipper()
    @city = City.find(params[:id])
  end

  ##
  # Attaches a new skipper to the given city.

  def attach_skipper()
    @city = City.find(params[:id])

    if @city.skippers << Skipper.find(params[:skipper_id])
      redirect_to @city
    else
      render :show_attach_skipper, status: :unprocessable_entity
    end
  end

  ##
  # Validates the query parameters.

  private def validate_params
    params.require(:city).permit!
  end
end
