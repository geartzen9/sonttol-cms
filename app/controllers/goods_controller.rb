##
# A controller class for the goods model.

class GoodsController < ApplicationController
  before_action :authenticate_user!

  ##
  # Shows all goods from the database on a view.

  def index
    @goods = Good.all
  end

  ##
  # Shows a single good from the database on a view.

  def show
    @good = Good.find(params[:id])
  end

  ##
  # Creates a new instance of a good and shows the form.

  def new
    @good = Good.new
  end

  ##
  # Creates a new good in the database and redirects to the the show view of that good.

  def create
    @good = Good.new(validate_params)

    if @good.save
      redirect_to @good
    else
      render :new, status: :unprocessable_entity
    end
  end

  ##
  # Finds the correct instance that needs to be updated and shows the update form.

  def edit
    @good = Good.find(params[:id])
  end

  ##
  # Checks if the parameters are valid and updates the instance.

  def update
    @good = Good.find(params[:id])

    if @good.update(validate_params)
      redirect_to @good
    else
      render :edit, status: :unprocessable_entity
    end
  end

  ##
  # Deletes the given good from the database.
  
  def destroy
    @good = Good.find(params[:id])
    @good.destroy

    redirect_to root_path, status: :see_other
  end

  ##
  # Validates the incoming parameters of the form.

  private def validate_params
    params.require(:good).permit(:type)
  end
end