## 
# A controller class for the passages model.

class PassagesController < ApplicationController
  before_action :authenticate_user!

  ##
  # Shows all passages from the database on a view.
  
  def index
    @passages = Passage.all
  end

  ##
  # Shows a single passage from the database on a view.

  def show
    @passage = Passage.find(params[:id])
  end

  ##
  # Creates a new instance of a passage and shows the form.

  def new
    @passage = Passage.new
  end

  ##
  # Creates a new passage in the database and redirects to the the show view of that passage.

  def create
    @passage = Passage.new(validate_params)

    if @passage.save
      redirect_to @passage
    else
      render :new, status: :unprocessable_entity
    end
  end

  ##
  # Finds the correct instance that needs to be updated and shows the update form.

  def edit
    @passage = Passage.find(params[:id])
  end

  ##
  # Checks if the parameters are valid and updates the instance.

  def update
    @passage = Passage.find(params[:id])

    if @passage.update(validate_params)
      redirect_to @passage
    else
      render :edit, status: :unprocessable_entity
    end
  end

  ##
  # Shows the attach form for attaching a new cargo to a passage.

  def show_attach_cargo()
    @passage = Passage.find(params[:id])
  end

  ##
  # Attaches a new cargo to the given passage.

  def attach_cargo()
    @passage = Passage.find(params[:id])

    if @passage.cargo.create(good_id: params[:good_id], measure: params[:measure], number_of_units: params[:number_of_units])
      redirect_to @passage
    else
      render :show_attach_cargo, status: :unprocessable_entity
    end
  end

  ##
  # Detaches the given city which the passage is from.
  
  def detach_from_city
    @passage = Passage.find(params[:id])
    from_city = @passage.from_city
    
    if @passage.update(:from_city_id => nil)
      redirect_to from_city
    else
      render :new, status: :unprocessable_entity
    end
  end

  ##
  # Detaches the given city which the passage is going to.
  
  def detach_to_city
    @passage = Passage.find(params[:id])
    to_city = @passage.to_city
    
    if @passage.update(:to_city_id => nil)
      redirect_to to_city
    else
      render :new, status: :unprocessable_entity
    end
  end

  ##
  # Deletes the given passage from the database.
  
  def destroy
    @passage = Passage.find(params[:id])
    @passage.destroy

    redirect_to root_path, status: :see_other
  end

  ##
  # Validates the query parameters.

  private def validate_params
    params.require(:passage).permit!
  end
end
