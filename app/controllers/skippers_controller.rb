##
# A controller class for the skippers model

class SkippersController < ApplicationController
  before_action :authenticate_user!
  
  ##
  # Show all skippers from the database on a view.
  
  def index
    @skippers = Skipper.all
  end

  ##
  # Show a single skipper with the given ID from the database on a view.

  def show
    @skipper = Skipper.find(params[:id])
  end

  ##
  # Creates a new instance of a skipper and shows the form.

  def new
    @skipper = Skipper.new
  end

  ##
  # Creates a new skipper in the database and redirects to the the show view of that skipper.

  def create
    @skipper = Skipper.new(
      name: params[:skipper][:name],
      city_id: params[:skipper][:city_id]
    )

    if @skipper.save
      redirect_to @skipper
    else
      render :new, status: :unprocessable_entity
    end
  end

  ##
  # Finds the correct instance that needs to be updated and shows the update form.

  def edit
    @skipper = Skipper.find(params[:id])
  end

  ##
  # Checks if the parameters are valid and updates the instance.

  def update
    @skipper = Skipper.find(params[:id])

    if @skipper.update(validate_params)
      redirect_to @skipper
    else
      render :edit, status: :unprocessable_entity
    end
  end

  ##
  # Detaches the given skipper from the city.
  
  def detach_city
    @skipper = Skipper.find(params[:id])
    city = @skipper.city
    
    if @skipper.update(:city_id => nil)
      redirect_to city
    else
      render :new, status: :unprocessable_entity
    end
  end

  ##
  # Deletes the given skipper from the database.
  
  def destroy
    @skipper = Skipper.find(params[:id])
    @skipper.destroy

    redirect_to root_path, status: :see_other
  end

  ##
  # Validates the query parameters.

  private def validate_params
    params.require(:skipper).permit!
  end
end
