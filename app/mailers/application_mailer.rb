class ApplicationMailer < ActionMailer::Base
  default from: "info@tresoar.nl"
  layout "mailer"
end
