class City < ApplicationRecord
    # Relations for from and to cities.
    has_many :from_passages, class_name: 'Passage', foreign_key: 'from_city_id', dependent: :nullify
    has_many :to_passages, class_name: 'Passage', foreign_key: 'to_city_id', dependent: :nullify

    # Relation for skippers
    has_many :skippers, dependent: :nullify

    # Validations
    validates :name, :latitude, :longitude, presence: true

    ##
    # Converts the current instance of City to a simple JSON object.

    def to_simple_json()
        return {
            "id": id,
            "name": name,
            "latitude": latitude,
            "longitude": longitude
        }
    end

    ##
    # Converts the current instance of City to a detailed JSON object.

    def to_detailed_json()
        captains = []
        routes = []

        # Loop through the skippers to convert to JSON.
        skippers.each do |skipper|
            captains.append(skipper.to_json)
        end

        # Loop through the from passages to convert to JSON.
        from_passages.each do |passage|
            routes.append(passage.to_json)
        end

        return {
            "id": id,
            "name": name,
            "captains": captains,
            "routes": routes
        }
    end
end
