class Good < ApplicationRecord
    self.inheritance_column = 'inheritance_type'

    has_many :cargo
    has_many :passages, through: :cargo, dependent: :destroy
end
