class Passage < ApplicationRecord
    # Relation with goods table using an association table called "cargos".
    has_many :cargo
    has_many :goods, through: :cargo, dependent: :destroy

    # Relation with skippers table.
    belongs_to :skipper

    # Relation with cities table.
    belongs_to :from_city, class_name: 'City', optional: true
    belongs_to :to_city, class_name: 'City', optional: true

    # Validation.
    validates :skipper_id, presence: true
    validates :tonnage, presence: true
    validates :serial_number, presence: true

    ##
    # Converts the current instance of City to a simple JSON object.

    def to_json()
        return {
            "shipment_number": serial_number,
            "skipper_name": skipper.name,
            "distance": "[FILL]",
            "departure_date": "[FILL]",
            "arrival_date": "[FILL]",
            "main_cargo": "[FILL]",
            "main_route": "[FILL]"
        }
    end
end