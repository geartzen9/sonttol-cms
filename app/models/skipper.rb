class Skipper < ApplicationRecord
    # Relation for passages.
    has_many :passages, dependent: :destroy

    # Relation for skipper.
    belongs_to :city, optional: true

    # Validations
    validates :name, presence: true

    ##
    # Converts the current instance of City to a JSON object.

    def to_json()
        return {
            "name": name,
            "main_cargo": "[FILL]",
            "total_distance": "[FILL]",
            "main_route": "[FILL]",
            "first_route_date": "[FILL]",
            "last_route_date": "[FILL]"
        }
    end
end
