Rails.application.routes.draw do
  root to: "application#welcome"

  devise_for :users

  # ---------------------------
  # -        Skippers         -
  # ---------------------------

  resources :skippers

  # Route for detaching a city from the given skipper.
  post "/skippers/:id/detach_city", to: "skippers#detach_city", as: :detach_city

  # ---------------------------
  # -         Cities          -
  # ---------------------------

  resources :cities

  # Route for showing the attaching form for skippers.
  get "/cities/:id/attach_skipper", to: "cities#show_attach_skipper"

  # Route for attaching a skipper to the given city.
  post "/cities/:id/attach_skipper", to: "cities#attach_skipper"

  # ---------------------------
  # -        Passages         -
  # ---------------------------

  resources :passages

  # Route for showing the attaching form for cargos.
  get "/passages/:id/attach_cargo", to: "passages#show_attach_cargo"

  # Route for attaching a cargo to the given city.
  post "/passages/:id/attach_cargo", to: "passages#attach_cargo"

  # Route for detaching a city which the passage is from.
  post "/passages/:id/detach_from_city", to: "passages#detach_from_city", as: :detach_from_city

  # Route for detaching a city which the passage is going to.
  post "/passages/:id/detach_to_city", to: "passages#detach_to_city", as: :detach_to_city

  # ---------------------------
  # -         Goods           -
  # ---------------------------

  resources :goods

  # ---------------------------
  # -         Cargo           -
  # ---------------------------

  resources :cargo

  # ---------------------------
  # -       API Routes        -
  # ---------------------------

  namespace :api do
    # Index for cities
    get "/cities", to: "cities#index"

    # Showing a single city
    get "/cities/:id", to: "cities#show"
  end
end
