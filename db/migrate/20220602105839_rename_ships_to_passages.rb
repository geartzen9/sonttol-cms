class RenameShipsToPassages < ActiveRecord::Migration[7.0]
  def change
    # Remove the ships table
    drop_table(:ships)

    # Create a new table called passages.
    create_table :passages do |t|
      t.decimal :tonnage, null: false
      t.integer :serial_number, null: false
      t.string :total_coin_type_1, null: true
      t.decimal :total_amount_1, null: true
      t.string :total_coin_type_2, null: true
      t.decimal :total_amount_2, null: true
      t.string :total_coin_type_3, null: true
      t.decimal :total_amount_3, null: true
      t.string :total_coin_type_4, null: true
      t.decimal :total_amount_4, null: true
      t.string :total_coin_type_5, null: true
      t.decimal :total_amount_5, null: true

      t.timestamps
    end
  end
end
