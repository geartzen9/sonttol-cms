class AddColumnsToSkippers < ActiveRecord::Migration[7.0]
  def change
    rename_column(:skippers, :name, :firstname)
    
    add_column(:skippers, :insertion, :string)
    add_column(:skippers, :lastname, :string)
  end
end
