class AddLatLongToCities < ActiveRecord::Migration[7.0]
  def change
    add_column :cities, :latitude, :decimal, null: false
    add_column :cities, :longitude, :decimal, null: false
  end
end
