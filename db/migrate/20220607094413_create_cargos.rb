class CreateCargos < ActiveRecord::Migration[7.0]
  def change
    create_table :cargos do |t|
      t.belongs_to :good
      t.belongs_to :passage

      t.integer :measure
      t.integer :number_of_units

      t.timestamps
    end
  end
end
