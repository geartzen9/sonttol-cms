class AddCityAssociationsToPassage < ActiveRecord::Migration[7.0]
  def change
    add_reference :passages, :from_city, null: true
    add_reference :passages, :to_city, null: true

    add_foreign_key :passages, :cities, column: :from_city_id
    add_foreign_key :passages, :cities, column: :to_city_id
  end
end
