class AddAssociationToCitiesInSkipper < ActiveRecord::Migration[7.0]
  def change
    change_table :skippers do |t|
      t.belongs_to :city, null: true, foreign_key: true
    end
  end
end
