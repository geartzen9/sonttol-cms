class ChangePassageSkipperAssociation < ActiveRecord::Migration[7.0]
  def change
    change_table :passages do |t|
      t.belongs_to :skipper, null: false, foreign_key: true
    end
  end
end
