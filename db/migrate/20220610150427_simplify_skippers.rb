class SimplifySkippers < ActiveRecord::Migration[7.0]
  def change
    remove_column :skippers, :firstname
    remove_column :skippers, :insertion
    remove_column :skippers, :lastname
    
    add_column :skippers, :name, :string
  end
end
