class ChangeSerialNumberOfPassage < ActiveRecord::Migration[7.0]
  def change
    change_column :passages, :serial_number, :string
  end
end
