class AddDatesToPassages < ActiveRecord::Migration[7.0]
  def change
    change_table :passages do |t|
      t.date :departure_date, null: true
      t.date :arrival_date, null: true
      t.belongs_to :main_cargo_id, null: true
    end
  end
end
