class ChangeCargoColumnsToNotNullable < ActiveRecord::Migration[7.0]
  def change
    change_column :cargos, :measure, :integer, null: false
    change_column :cargos, :number_of_units, :integer, null: false
  end
end
