# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_06_14_123809) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cargos", force: :cascade do |t|
    t.bigint "good_id"
    t.bigint "passage_id"
    t.integer "measure", null: false
    t.integer "number_of_units", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["good_id"], name: "index_cargos_on_good_id"
    t.index ["passage_id"], name: "index_cargos_on_passage_id"
  end

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "latitude", null: false
    t.decimal "longitude", null: false
  end

  create_table "goods", force: :cascade do |t|
    t.string "type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "passages", force: :cascade do |t|
    t.decimal "tonnage", null: false
    t.string "serial_number", null: false
    t.string "total_coin_type_1"
    t.decimal "total_amount_1"
    t.string "total_coin_type_2"
    t.decimal "total_amount_2"
    t.string "total_coin_type_3"
    t.decimal "total_amount_3"
    t.string "total_coin_type_4"
    t.decimal "total_amount_4"
    t.string "total_coin_type_5"
    t.decimal "total_amount_5"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "from_city_id"
    t.bigint "to_city_id"
    t.bigint "skipper_id", null: false
    t.date "departure_date"
    t.date "arrival_date"
    t.bigint "main_cargo_id_id"
    t.index ["from_city_id"], name: "index_passages_on_from_city_id"
    t.index ["main_cargo_id_id"], name: "index_passages_on_main_cargo_id_id"
    t.index ["skipper_id"], name: "index_passages_on_skipper_id"
    t.index ["to_city_id"], name: "index_passages_on_to_city_id"
  end

  create_table "skippers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "city_id"
    t.string "name"
    t.index ["city_id"], name: "index_skippers_on_city_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "passages", "cities", column: "from_city_id"
  add_foreign_key "passages", "cities", column: "to_city_id"
  add_foreign_key "passages", "skippers"
  add_foreign_key "skippers", "cities"
end
