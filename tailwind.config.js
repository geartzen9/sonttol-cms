module.exports = {
  content: ["./app/**/*.{html,js,erb}"],
  theme: {
    extend: {
      colors: {
        'primary': '#a80b62',
        'secondary': '#b09116'
      }
    }
  },
  plugins: [],
}
